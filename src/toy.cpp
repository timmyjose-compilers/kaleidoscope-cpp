#include <cctype>
#include <cstdio>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

enum Token { Eof = -1, Def = -2, Extern = -3, Identifier = -4, Number = -5 };

static std::string id_str;
static double num_val;

static int get_tok() {
  static int last_char = ' ';

  while (std::isspace(last_char)) {
    last_char = getchar();
  }

  if (std::isalpha(last_char)) {
    id_str = last_char;
    while (std::isalnum(last_char = getchar())) {
      id_str += last_char;
    }

    if (id_str == "def") {
      return Token::Def;
    } else if (id_str == "extern") {
      return Token::Extern;
    }

    return Token::Identifier;
  } else if (std::isdigit(last_char) || last_char == '.') {
    std::string num_str;

    do {
      num_str += last_char;
      last_char = getchar();
    } while (std::isdigit(last_char) || last_char == '.');

    num_val = std::stod(num_str.c_str(), 0);
    return Token::Number;
  } else if (last_char == '#') {
    do {
      last_char = getchar();
    } while (last_char != EOF && last_char != '\n' && last_char != '\r');

    if (last_char != EOF) {
      return get_tok();
    }
  } else if (last_char == EOF) {
    return Token::Eof;
  }

  int this_char = last_char;
  last_char = getchar();

  return this_char;
}

class ExprAST {
public:
  virtual ~ExprAST() {}
};

class NumberExprAST : public ExprAST {
public:
  NumberExprAST(double val) : val(val) {}

private:
  double val;
};

class VariableExprAST : public ExprAST {
public:
  VariableExprAST(const std::string &name) : name(name) {}

private:
  std::string name;
};

class BinaryExprAST : public ExprAST {
public:
  BinaryExprAST(char op, std::unique_ptr<ExprAST> lhs,
                std::unique_ptr<ExprAST> rhs)
      : op(op), lhs(std::move(lhs)), rhs(std::move(rhs)) {}

private:
  char op;
  std::unique_ptr<ExprAST> lhs;
  std::unique_ptr<ExprAST> rhs;
};

class CallExprAST : public ExprAST {
public:
  CallExprAST(const std::string &callee,
              std::vector<std::unique_ptr<ExprAST>> args)
      : callee(callee), args(std::move(args)) {}

private:
  std::string callee;
  std::vector<std::unique_ptr<ExprAST>> args;
};

class PrototypeAST {
public:
  PrototypeAST(const std::string &name, std::vector<std::string> args)
      : name(name), args(std::move(args)) {}

  const std::string &get_name() const { return name; }

private:
  std::string name;
  std::vector<std::string> args;
};

class FunctionAST {
public:
  FunctionAST(std::unique_ptr<PrototypeAST> proto,
              std::unique_ptr<ExprAST> body)
      : proto(std::move(proto)), body(std::move(body)) {}

private:
  std::unique_ptr<PrototypeAST> proto;
  std::unique_ptr<ExprAST> body;
};

static int cur_tok;

static int get_next_token() {
  cur_tok = get_tok();
  return cur_tok;
}

std::unique_ptr<ExprAST> log_error(const char *s) {
  fprintf(stderr, "error: %s\n", s);
  return nullptr;
}

std::unique_ptr<PrototypeAST> log_error_p(const char *s) {
  log_error(s);
  return nullptr;
}

// number-expr ::= number
static std::unique_ptr<ExprAST> parse_number_expr() {
  auto res = std::make_unique<NumberExprAST>(num_val);
  get_next_token();
  return res;
}

// paren_expr ::= '(' expr ')'
static std::unique_ptr<ExprAST> parse_paren_expr() {
  get_next_token();
  auto expr = parse_expression();
  if (!expr) {
    return nullptr;
  }

  if (cur_tok != ')') {
    log_error("expected ')'");
  }
  get_next_token();

  return expr;
}

// identifier-expr ::= identifier
//                   | identifier '(' expression * ')'
static std::unique_ptr<ExprAST> parse_identifier_expr() {}

int main(int argc, char *argv[]) {
  std::cout << "Welcome to Kaleidoscope!" << std::endl;

  int kind = Token::Eof;
  do {
    int kind = get_tok();
    if (kind == Token::Identifier) {
      std::cout << "Got an identifier: " << id_str << std::endl;
    } else if (kind == Token::Number) {
      std::cout << "Got a number " << num_val << std::endl;
    } else {
      std::cout << "Got a token of kind " << kind << std::endl;
    }
  } while (kind != Token::Eof);

  return 0;
}